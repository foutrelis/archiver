package main

import (
	"compress/gzip"
	"encoding/base64"
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestFetchRepo(t *testing.T) {
	repoDBURL, _ := getRepoDBURL(defaultMirrorURL, "core", defaultArch)

	repoDB, lastMod, _ := fetchRepo(repoDBURL, time.Time{})
	assert.GreaterOrEqual(t, len(repoDB), 100*1024)

	packages, _, err := fetchRepo(repoDBURL, lastMod)
	if assert.NoError(t, err) {
		assert.Nil(t, packages, "downloaded unmodified repository")
	}

	_, _, err = fetchRepo("cat://invalid-scheme", time.Time{})
	assert.ErrorContains(t, err, "unsupported protocol scheme")

	_, _, err = fetchRepo(repoDBURL+".missing", time.Time{})
	assert.ErrorContains(t, err, "404 Not Found")
}

func TestListRepo(t *testing.T) {
	repoDBURL, _ := getRepoDBURL(defaultMirrorURL, "core", defaultArch)

	repoDB, _, err := fetchRepo(repoDBURL, time.Time{})
	if assert.NoError(t, err) {
		packages, err := listRepo(repoDB)
		if assert.NoError(t, err) {
			foundPacman := false
			for _, pkg := range packages {
				if pkg.Name == "pacman" {
					foundPacman = true
					break
				}
			}
			if !foundPacman {
				assert.Fail(t, "returned list did not contain pacman")
			}
		}
	}

	_, err = listRepo([]byte("definitely not a gzip archive"))
	assert.ErrorIs(t, err, gzip.ErrHeader)

	emptyGzip, _ := base64.RawStdEncoding.DecodeString("H4sIAAAAAAAAAwMAAAAAAAAAAAA=")
	_, err = listRepo(emptyGzip)
	assert.EqualError(t, err, "failed to read the tar archive")
}

func TestArchivePackages(t *testing.T) {
	downloads := 0
	var fakeDownloadFile = func(fileURL string, fileDest string) error {
		assert.Contains(t, []string{
			"https://example.com/foo.pkg",
			"https://example.com/foo.pkg.sig",
		}, fileURL)
		assert.Contains(t, []string{
			"packages/f/foo/foo.pkg",
			"packages/f/foo/foo.pkg.sig",
		}, fileDest)
		downloads++
		return nil
	}

	packages := []repoPackage{
		{Filename: "foo.pkg", Name: "foo"},
	}
	archivePackages(packages, "https://example.com", fakeDownloadFile)
	assert.Equal(t, 2*len(packages), downloads)
}

func TestListSources(t *testing.T) {
	sourceGroupURL, _ := url.JoinPath(defaultSourceURL, "packages")

	sources, err := listSources(sourceGroupURL)
	if assert.NoError(t, err) {
		assert.GreaterOrEqual(t, len(sources), 1000)
	}

	invalidSourceGroupURL, _ := url.JoinPath(defaultSourceURL, "invalid")
	_, err = listSources(invalidSourceGroupURL)
	assert.Error(t, err)

	_, err = listSources("cat://invalid-scheme")
	assert.ErrorContains(t, err, "unsupported protocol scheme")
}

func TestArchiveSources(t *testing.T) {
	downloads := 0
	var fakeDownloadFile = func(fileURL string, fileDest string) error {
		assert.Contains(t, fileURL, "https://sources.archlinux.org/sources/packages/")
		assert.Contains(t, fileURL, ".src.tar.gz")
		assert.Contains(t, fileDest, "sources/packages/")
		assert.Contains(t, fileDest, ".src.tar.gz")
		assert.Contains(t, fileURL, fileDest)
		downloads++
		return nil
	}

	archiveSources("packages", defaultSourceURL, fakeDownloadFile)
	assert.GreaterOrEqual(t, downloads, 1000)
}
