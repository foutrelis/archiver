package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"time"
)

func parseLastModified(resp *http.Response) (time.Time, error) {
	return time.Parse(time.RFC1123, resp.Header.Get("Last-Modified"))
}

func downloadFile(fileURL string, fileDest string) error {
	err := os.MkdirAll(path.Dir(fileDest), 0755)
	if err != nil && !os.IsExist(err) {
		return err
	}

	if _, err := os.Stat(fileDest); err == nil {
		return nil
	}

	tmpFileDest := fileDest + ".part"
	tmpFile, err := os.OpenFile(tmpFileDest, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer func() {
		tmpFile.Close()
		os.Remove(tmpFileDest)
	}()

	log.Printf("downloading %v\n", fileURL)
	resp, err := http.Get(fileURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("download failed (%v)", resp.Status)
	}

	_, err = io.Copy(tmpFile, resp.Body)
	if err != nil {
		return err
	}

	if lastMod, err := parseLastModified(resp); err == nil {
		os.Chtimes(tmpFileDest, lastMod, lastMod)
	}

	tmpFile.Sync()
	tmpFile.Close()
	os.Rename(tmpFileDest, fileDest)

	return nil
}

func getRepoURL(mirrorURL string, repo string, arch string) string {
	mirrorURL = strings.ReplaceAll(mirrorURL, "$repo", repo)
	mirrorURL = strings.ReplaceAll(mirrorURL, "$arch", arch)
	return mirrorURL
}

func getRepoDBURL(mirrorURL string, repo string, arch string) (string, error) {
	return url.JoinPath(getRepoURL(mirrorURL, repo, arch), repo+".db.tar.gz")
}
