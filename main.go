package main

import (
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/urfave/cli/v2"
)

const (
	defaultMirrorURL = "https://geo.mirror.pkgbuild.com/$repo/os/$arch"
	defaultSourceURL = "https://sources.archlinux.org/sources"
	defaultArch      = "x86_64"
)

var (
	archiveRoot  string
	defaultRepos = []string{
		"community", "community-staging", "community-testing",
		"core", "extra", "gnome-unstable", "kde-unstable",
		"multilib", "multilib-staging", "multilib-testing",
		"staging", "testing",
	}
)

func main() {
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:  "pull",
				Usage: "Fetches and archives packages",
				Flags: []cli.Flag{
					&cli.DurationFlag{
						Name: "interval", Aliases: []string{"i"},
						Usage: "check for new packages every `INTERVAL`",
					},
					&cli.StringFlag{
						Name: "repo", Aliases: []string{"r"},
						Usage: "fetch packages from a single `REPO` only",
					},
					&cli.BoolFlag{
						Name: "sources", Usage: "include package sources",
					},
				},
				Action: func(cCtx *cli.Context) error {
					var repos []string
					switch cCtx.String("repo") {
					case "":
						repos = defaultRepos
					default:
						repos = []string{cCtx.String("repo")}
					}

					lastMod := make(map[string]time.Time)
					for _, repo := range repos {
						lastMod[repo] = time.Time{}
					}

				daemonLoop:
					for {
						for _, repo := range repos {
							repoDBURL, _ := getRepoDBURL(defaultMirrorURL, repo, defaultArch)

							repoDB, newLastMod, err := fetchRepo(repoDBURL, lastMod[repo])
							if err != nil {
								log.Print(err)
								continue
							}
							if repoDB == nil {
								continue
							}

							packages, err := listRepo(repoDB)
							if err != nil {
								log.Print(err)
								continue
							}

							lastMod[repo] = newLastMod
							repoURL := getRepoURL(defaultMirrorURL, repo, defaultArch)
							archivePackages(packages, repoURL, downloadFile)
						}

						if cCtx.Bool("sources") {
							archiveSources("packages", defaultSourceURL, downloadFile)
							archiveSources("community", defaultSourceURL, downloadFile)
						}

						interval := cCtx.Duration("interval")
						switch interval {
						case 0:
							break daemonLoop
						default:
							time.Sleep(interval)
						}
					}
					return nil
				},
			},
			{
				Name:  "snapshot",
				Usage: "Stores a daily snapshot of the repo dbs",
				Action: func(cCtx *cli.Context) error {
					for _, repo := range defaultRepos {
						repoDBURL := getRepoURL(defaultMirrorURL, repo, defaultArch)
						snapshotPath := filepath.FromSlash(time.Now().Format("2006/01/02"))
						snapshotRepo(repo, repoDBURL, snapshotPath, downloadFile, os.Symlink)
					}
					return nil
				},
			},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name: "archive", Aliases: []string{"a"}, Value: "/srv/archive",
				Usage: "use `DIRECTORY` as the archive root",
			},
		},
		Before: func(cCtx *cli.Context) error {
			archiveRoot = cCtx.String("archive")
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
