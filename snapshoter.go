package main

import (
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func snapshotRepo(repo string, repoURL string, snapshotPath string,
	downloadFunc func(fileURL string, fileDest string) error,
	symlinkFunc func(string, string) error) {

	repoFilename := repo + ".db.tar.gz"
	fileURL, _ := url.JoinPath(repoURL, repoFilename)
	fileDest := filepath.Join(archiveRoot, "repos", snapshotPath, repo, "os", "x86_64", repoFilename)
	if err := downloadFunc(fileURL, fileDest); err != nil {
		log.Print(err)
	}

	err := symlinkFunc(path.Base(fileDest), strings.TrimSuffix(fileDest, ".tar.gz"))
	if err != nil && !os.IsExist(err) {
		log.Print(err)
	}
}
