package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSnapshotRepo(t *testing.T) {
	downloads := 0
	var fakeDownloadFile = func(fileURL string, fileDest string) error {
		assert.Equal(t, "https://example.com/foo.db.tar.gz", fileURL)
		assert.Equal(t, "repos/test/foo/os/x86_64/foo.db.tar.gz", fileDest)
		downloads++
		return nil
	}

	symlinks := 0
	var fakeSymlink = func(oldname string, newname string) error {
		assert.Equal(t, "foo.db.tar.gz", oldname)
		assert.Equal(t, "repos/test/foo/os/x86_64/foo.db", newname)
		symlinks++
		return nil
	}

	snapshotRepo("foo", "https://example.com", "test", fakeDownloadFile, fakeSymlink)
	assert.Equal(t, 1, downloads)
	assert.Equal(t, 1, symlinks)
}
