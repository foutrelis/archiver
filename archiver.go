package main

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"time"
)

type repoPackage struct {
	Filename string
	Name     string
}

func fetchRepo(repoDBURL string, lastMod time.Time) ([]byte, time.Time, error) {
	if resp, err := http.Head(repoDBURL); err == nil {
		newLastMod, err := parseLastModified(resp)
		if err == nil && lastMod.Equal(newLastMod) {
			return nil, lastMod, nil
		}
	}

	log.Printf("fetching %v\n", repoDBURL)
	resp, err := http.Get(repoDBURL)
	if err != nil {
		return nil, lastMod, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, lastMod, fmt.Errorf("fetch failed (%v)", resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, lastMod, err
	}

	newLastMod, err := parseLastModified(resp)
	if err == nil {
		lastMod = newLastMod
	}

	return body, lastMod, nil
}

func listRepo(repoDB []byte) ([]repoPackage, error) {
	zr, err := gzip.NewReader(bytes.NewReader(repoDB))
	if err != nil {
		return nil, err
	}

	var packages []repoPackage
	tr := tar.NewReader(zr)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.New("failed to read the tar archive")
		}

		if !strings.HasSuffix(hdr.Name, "/desc") {
			continue
		}

		desc, err := io.ReadAll(tr)
		if err != nil {
			return nil, err
		}

		repoPackageType := reflect.TypeOf(repoPackage{})
		packageAttributes := make([]string, repoPackageType.NumField())
		for i := range packageAttributes {
			packageAttributes[i] = repoPackageType.Field(i).Name
		}

		lines := strings.Split(string(desc), "\n")
		pkg := repoPackage{}
		foundAttribute := ""
		for _, line := range lines {
			if foundAttribute != "" {
				reflect.ValueOf(&pkg).Elem().FieldByName(foundAttribute).SetString(line)
				foundAttribute = ""
			}

			for _, attr := range packageAttributes {
				if fmt.Sprintf("%%%v%%", strings.ToUpper(attr)) == line {
					foundAttribute = attr
					break
				}
			}
		}

		for _, attr := range packageAttributes {
			if reflect.ValueOf(pkg).FieldByName(attr).String() == "" {
				return nil, fmt.Errorf("invalid package entry %v, missing %v", hdr.Name, attr)
			}
		}

		packages = append(packages, pkg)
	}

	return packages, nil
}

func archivePackages(packages []repoPackage, repoURL string, downloadFunc func(fileURL string, fileDest string) error) {
	for _, pkg := range packages {
		fileURL, _ := url.JoinPath(repoURL, pkg.Filename)
		fileDest := filepath.Join(archiveRoot, "packages", pkg.Name[:1], pkg.Name, pkg.Filename)
		if err := downloadFunc(fileURL, fileDest); err != nil {
			log.Print(err)
		}

		fileURL += ".sig"
		fileDest += ".sig"
		if err := downloadFunc(fileURL, fileDest); err != nil {
			log.Print(err)
		}
	}
}

func listSources(sourceURL string) ([]string, error) {
	resp, err := http.Get(sourceURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("fetch failed (%v)", resp.Status)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	hrefRegex := regexp.MustCompile(`href="([^"]+\.src\.tar\.gz)"`)
	matches := hrefRegex.FindAllStringSubmatch(string(body), -1)

	var sources []string
	for _, match := range matches {
		sources = append(sources, match[1])
	}

	return sources, nil
}

func archiveSources(sourceGroup string, sourceURL string, downloadFunc func(fileURL string, fileDest string) error) {
	sourceGroupURL, _ := url.JoinPath(sourceURL, sourceGroup)
	sources, err := listSources(sourceGroupURL)
	if err != nil {
		log.Print(err)
		return
	}

	for _, source := range sources {
		fileURL, _ := url.JoinPath(sourceGroupURL, source)
		fileDest := filepath.Join(archiveRoot, "sources", sourceGroup, source)
		if err := downloadFunc(fileURL, fileDest); err != nil {
			log.Print(err)
		}
	}
}
