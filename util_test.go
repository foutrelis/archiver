package main

import (
	"os"
	"os/user"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func getFileSize(filePath string) int64 {
	fileInfo, _ := os.Stat(filePath)
	return fileInfo.Size()
}

func TestDownloadFile(t *testing.T) {
	fileURL, _ := getRepoDBURL(defaultMirrorURL, "core", defaultArch)

	destDir := t.TempDir()
	fileDest := path.Join(destDir, "core.db.tar.gz")

	err := downloadFile("cat://invalid-scheme", fileDest)
	assert.ErrorContains(t, err, "unsupported protocol scheme")

	err = downloadFile(fileURL+".missing", fileDest)
	assert.ErrorContains(t, err, "404 Not Found")

	downloadFile(fileURL, fileDest)
	assert.GreaterOrEqual(t, getFileSize(fileDest), int64(100*1024))

	os.Truncate(fileDest, 0)
	downloadFile(fileURL, fileDest)
	assert.Zero(t, getFileSize(fileDest), "Clobbered existing file")
}

func TestDownloadFilePermissionDenied(t *testing.T) {
	if user, err := user.Current(); err == nil {
		if user.Username == "root" {
			t.Skip("Permission tests don't work when running as root")
		}
	}
	fileURL, _ := getRepoDBURL(defaultMirrorURL, "core", defaultArch)
	readOnlyDir := t.TempDir()
	os.Chmod(readOnlyDir, 0555)

	err := downloadFile(fileURL, path.Join(readOnlyDir, "testdir", "testfile"))
	assert.EqualError(t, err, "mkdir "+path.Join(readOnlyDir, "testdir")+": permission denied")

	err = downloadFile(fileURL, path.Join(readOnlyDir, "testfile"))
	assert.EqualError(t, err, "open "+path.Join(readOnlyDir, "testfile.part")+": permission denied")
}

func TestGetRepoURL(t *testing.T) {
	repoURL := getRepoURL("https://example.com/$repo/os/$arch", "core", "x86_64")
	assert.Equal(t, "https://example.com/core/os/x86_64", repoURL)
}

func TestGetRepoDBURL(t *testing.T) {
	repoDBURL, err := getRepoDBURL("https://example.com/$repo/os/$arch", "core", "x86_64")
	if assert.NoError(t, err) {
		assert.Equal(t, "https://example.com/core/os/x86_64/core.db.tar.gz", repoDBURL)
	}
}
